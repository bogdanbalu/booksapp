<?php

namespace Database\Factories;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class BookFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $userId = User::where('role_id', Role::USER)->pluck('id');
        return [
            'title' => $this->faker->name(),
            'author' => $this->faker->name(),
            'user_id' => collect($userId)->random(),
            'release_date' =>$this->faker->dateTimeThisMonth()->format('Y-m-d'),
        ];
    }
}
