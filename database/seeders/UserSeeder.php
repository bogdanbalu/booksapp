<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
            [
                'id' => 1,
                'first_name' => 'System',
                'last_name' => 'Admin',
                'role_id' => Role::SYSTEM_ADMIN,
                'email' => 'system_admin@booksapp.com',
                'password' => bcrypt('Quantum7*'),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ]);
    }
}
