<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\BookController;
use App\Http\Controllers\Web\AdminController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[LoginController::class, 'redirectTo']);

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::prefix('/admin')->group(function() {
        Route::get('/register', [AdminController::class, 'showRegisterForm']);
        Route::post('/register/user', [AdminController::class, 'register'])->name('register');
        Route::get('/user/list', [AdminController::class, 'showUserList']);
        Route::delete('/delete/{user}', [AdminController::class, 'delete'])->name('delete');
    });
    Route::prefix('/user')->group(function() {
        Route::prefix('/books')->group(function() {
            Route::get('/list', [BookController::class, 'showUserBooks']);
            Route::get('/all', [BookController::class, 'showBooks']);
            Route::get('/create/form', [BookController::class, 'showBookForm']);
            Route::post('/create', [BookController::class, 'createBooks'])->name('create');
            Route::delete('/delete/{book}', [BookController::class, 'delete'])->name('delete');
        });
    });
});