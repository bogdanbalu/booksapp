## Installation

First clone this repository, install the dependencies, and setup your .env file.

```
git clone https://bogdanbalu@bitbucket.org/bogdanbalu/booksapp.git
composer install
cp .env.example .env
```

Run the initial migrations and seeders.

```
php artisan migrate:fresh --seed
```

## Specifications

A small project which must have functionalities to add information about books and extract them through an API

1. Users are connected to an account (account has multiple users)
2. Login page, application can be accessed only by logged users
3. A page with a form to add a book (title, author, release date), the insert must be unique by author and title. Inserted book belongs to user account.
4. A page where user can list (only) books added to his account in a table. User can see books added by other users from same account.
5. In table must be a column with “Delete” button, to delete a book - delete action is accessible only first 2 days after book was inserted.
6. API endpoint used by an account to fetch (only) its books. (/api/books)
7. API endpoint to get information of a specific book by id (/api/books/1)
8. API is secured with Basic Authentication (username, password)
9. Middleware to log responses (account, status code, content) from "7. API endpoint to get information"
10. Seeder to create demo accounts, users, books (Optional)