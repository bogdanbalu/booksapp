<link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3 class="title">User</h3>
        </div>

        <ul class="list-unstyled components">
            <li class="active">
                <li>
                    <a href="{{ url('/user/books/create/form') }}" class="text">Create Book</a>
                </li>
                <li>
                    <a href="{{ url('/user/books/list') }}" class="text">Your Books List</a>
                </li>
                <li>
                    <a href="{{ url('/user/books/all') }}" class="text" class="text">Books List</a>
                </li>
            </li>
        </ul>
    </nav>
</div>