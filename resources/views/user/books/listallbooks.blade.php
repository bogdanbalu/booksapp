@extends('layouts.app')
@section('content')
<link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                @include('user.sidebar')
            </div>
            <div class="col-sm-10">
                <h3 class="title">All books list</h3>
                <table class="table center">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Title</th>
                            <th scope="col">Author</th>
                            <th scope="col">Release Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($books as $key => $book)
                        <tr>
                            <th>{{ $key+ $books->firstItem() }}</th>
                            <td>{{ $book->title }}</td>
                            <td>{{ $book->author }}</td>
                            <td>{{ $book->release_date }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div style="float: right">
                    {{ $books->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection