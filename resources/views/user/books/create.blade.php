@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                @include('user.sidebar')
            </div>
            <div class="col-sm-10">
                <h3 class="title">Create Book</h3>
                @if (session()->has('status'))
                <div class="alert alert-success" role="alert">
                    <p style="text-align: center">{{ session()->get('message') }}
                    <button id="close" type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </p>
                </div>
                @endif
                <form class="row register-form" class="form-group" method="POST" action="{{ route('create') }}">
                    @csrf     
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input 
                                type="text" 
                                id="title" 
                                class="form-control" 
                                name="title" 
                                class="form-control @error('title') is-invalid @enderror"
                                required
                                placeholder="Title *" 
                                value="{{ old('title', $user->title ?? '') }}"/>
                            @if($errors->has('title'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Author</label>
                            <input 
                                type="text" 
                                id="author" 
                                class="form-control" 
                                name="author" 
                                class="form-control @error('author') is-invalid @enderror"
                                required
                                placeholder="Author *" 
                                value="{{ old('author', $user->author ?? '') }}"/>    
                            @if($errors->has('author'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('author') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Release Date</label>
                            <input 
                                type="date" 
                                id="release_date" 
                                class="form-control" 
                                name="release_date" 
                                class="form-control @error('release_date') is-invalid @enderror"
                                required
                                placeholder="Release Date *"
                                value="{{ old('release_date', $user->release_date ?? '') }}"/>
                            @if($errors->has('release_date'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('release_date') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary"  value="Save"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/book.js') }}"></script>
@endsection
