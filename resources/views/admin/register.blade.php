@extends('layouts.app')
@section('content')
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                @include('admin.sidebar');
            </div>
            <div class="col-sm-10">
                `<h3 class="title">Register</h3>
                @if (session()->has('status'))
                <div class="alert alert-success" role="alert">
                    <p style="text-align: center">{{ session()->get('message') }}
                    <button id="close" type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </p>
                </div>
                @endif
                <form class="row register-form form-group center" method="POST" action="{{ route('register') }}">
                    @csrf     
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="title">First name</label>
                            <input 
                                type="text" 
                                id="first_name" 
                                class="form-control" 
                                name="first_name" 
                                class="form-control @error('first_name') is-invalid @enderror"
                                required
                                placeholder="First Name *" 
                                value="{{ old('first_name', $user->first_name ?? '') }}"/>
                            @if($errors->has('first_name'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Last name</label>
                            <input 
                                type="text" 
                                id="last_name" 
                                class="form-control" 
                                name="last_name" 
                                class="form-control @error('last_name') is-invalid @enderror"
                                required
                                placeholder="Last Name *" 
                                value="{{ old('last_name', $user->last_name ?? '') }}"/>    
                            @if($errors->has('last_name'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Email</label>
                            <input 
                                type="email" 
                                id="email" 
                                class="form-control" 
                                name="email" 
                                class="form-control @error('email') is-invalid @enderror"
                                required
                                placeholder="Email *"
                                value="{{ old('email', $user->email ?? '') }}"/>
                            @if($errors->has('email'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Password</label>
                            <input 
                                type="password" 
                                id="password" 
                                class="form-control" 
                                name="password" 
                                class="form-control @error('password') is-invalid @enderror"
                                required
                                placeholder="Password *" />
                            @if($errors->has('password'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="title">Confirm Password</label>
                            <input 
                                type="password" 
                                id="password_confirmation" 
                                class="form-control" 
                                name="password_confirmation" 
                                class="form-control @error('password_confirmation') is-invalid @enderror"
                                required
                                placeholder="Confirm Password *" />
                            @if($errors->has('password_confirmation'))
                                <span class="text-danger" role="alert">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                        <input type="submit" class="btn btn-primary"  value="Register"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
