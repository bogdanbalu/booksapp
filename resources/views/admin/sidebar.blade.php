<link href="{{ asset('css/sidebar.css') }}" rel="stylesheet">
<div class="wrapper">
    <!-- Sidebar -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3 class="title">System Admin</h3>
        </div>
        <ul class="list-unstyled components">
            <li class="active">
                <li>
                    <a href="{{ url('/admin/register') }}" class="text">Register User</a>
                </li>
                <li>
                    <a href="{{ url('/admin/user/list') }}" class="text">User List</a>
                </li>
            </li>
        </ul>
    </nav>
</div>