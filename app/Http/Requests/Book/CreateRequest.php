<?php

namespace App\Http\Requests\Book;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
   /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check() && auth()->user()->role_id === Role::USER;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|unique:books,title',
            'author' => 'required|string|unique:books,author',
            'release_date' => 'required|date_format:Y-m-d|before:tomorrow'
        ];
    }
}
