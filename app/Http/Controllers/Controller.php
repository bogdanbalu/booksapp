<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response as ResponseStatus;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function response($data, $status, $message = '') {
        $result = [
            'result' => $data,
            'message' => $message
        ];
        if ($status < ResponseStatus::HTTP_OK || $status > ResponseStatus::HTTP_IM_USED) {
            $result = [
                'result' => $data,
                'message' => $message
            ];
        }
        return response()->json([
            $result,
            $status,
        ]);
    }
}
