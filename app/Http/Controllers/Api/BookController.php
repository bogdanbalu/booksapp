<?php

namespace App\Http\Controllers\Api;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use Symfony\Component\HttpFoundation\Response as ResponseStatus;

class BookController extends Controller
{
    /**
     * Get user books
     *
     * @return void
     */
    public function index() {
        $this->authorize('user', User::class);
        $books = Book::show(auth()->id())->get();
        return $this->response(BookResource::collection($books), ResponseStatus::HTTP_OK);
    }

    /**
     * Get book by id
     *
     * @param Book $book
     * @return void
     */
    public function show(Book $book) {
        $this->authorize('user', User::class);
        return $this->response(new BookResource($book), ResponseStatus::HTTP_OK);
    }
}
