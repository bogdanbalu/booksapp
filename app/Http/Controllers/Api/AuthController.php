<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\AccountResource;
use App\Http\Requests\Auth\LoginRequest;
use Symfony\Component\HttpFoundation\Response as ResponseStatus;

class AuthController extends Controller
{
    /**
     * Login
     *
     * @param LoginRequest $request
     * @return void
     */
    public function login(LoginRequest $request) {
        $user = User::where('email', $request->email)->first();
        if (Auth::attempt($request->validated())) {
            $token = $user->createToken('api_token')->plainTextToken;
            $data = array_merge(
                ['user' => new AccountResource($user)],
                ['token' => $token]
            );
            return $this->response($data, ResponseStatus::HTTP_OK, 'Successfully login');
        }
        return $this->response([], ResponseStatus::HTTP_UNAUTHORIZED, 'Invalid credentials');
    }

    /**
     * Logout
     *
     * @return void
     */
    public function logout() {
        $user = auth()->user();
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
        return $this->response([], ResponseStatus::HTTP_OK, 'Successfully logout');
    }
}
