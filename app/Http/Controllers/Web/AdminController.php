<?php

namespace App\Http\Controllers\Web;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Resources\AccountResource;
use App\Http\Requests\Admin\RegisterRequest;

class AdminController extends Controller
{
     /**
     * Register user form page
     *
     * @return void
     */
    public function showRegisterForm() {
        $this->authorize('admin', User::class);
        return view('admin.register');
    }

    /**
     * Create a user account with admin account
     *
     * @param RegisterRequest $request
     * @return void
     */
    public function register(RegisterRequest $request) {
        $this->authorize('admin', User::class);
        User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'created_by' => auth()->id(),
            'password' => bcrypt($request->password),
        ]);
        return back()->with(['status' => true, 'message' => 'Account successfully created']);
    }

    /**
     * Admin users list
     *
     * @return void
     */
    public function showUserList() {
        $this->authorize('admin', User::class);
        $users = User::where('created_by', auth()->id())->paginate(10);
        return view('admin.list', ['users' => AccountResource::collection($users)]);
    }

    /**
     * Delete a user
     *
     * @param User $user
     * @return void
     */
    public function delete(User $user) {
        $this->authorize('admin', User::class);
        $user->delete();
        return back();
    }
}
