<?php

namespace App\Http\Controllers\Web;

use App\Models\Book;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\BookResource;
use App\Http\Requests\Book\CreateRequest;

class BookController extends Controller
{
    /**
     * User book list
     *
     * @return void
     */
    public function showUserBooks() {
        $this->authorize('user', User::class);
        $books = Book::show(auth()->id())->paginate(10);
        $books = BookResource::collection($books);
        return view('user.books.list', ['books' => $books]);
    }

    /**
     * Create book form
     *
     * @return void
     */
    public function showBookForm() {
        $this->authorize('user', User::class);
        return view('user.books.create');
    }

    /**
     * Create book
     *
     * @param CreateRequest $request
     * @return void
     */
    public function createBooks(CreateRequest $request) {
        $this->authorize('user', User::class);
        Book::create([
            'user_id' => auth()->id(),
            'title' => $request->title,
            'author' => $request->author,
            'release_date' => $request->release_date
        ]);
        return back()->with(['status' => true, 'message' => 'Book created with success']);
    }

    /**
     * Delete book
     *
     * @param Book $book
     * @return void
     */
    public function delete(Book $book) {
        $book->delete();
        return back();
    }

    /**
     * Show all books
     *
     * @return void
     */
    public function showBooks() {
        $this->authorize('user', User::class);
        $userIds = User::where('created_by', auth()->user()->created_by)->pluck('id');
        $books = Book::whereIn('user_id', $userIds)->paginate(10);
        return view('user.books.listallbooks', ['books' => BookResource::collection($books)]);
    }
}
