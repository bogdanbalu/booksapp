<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Login
     *
     * @param LoginRequest $request
     * @return void
     */
    public function login(LoginRequest $request) {
        $user = User::where('email', $request->email)->first();
        if($user && Auth::attempt($request->validated())) {
            $request->session()->regenerate();
            if ($user->role_id === Role::SYSTEM_ADMIN) {
                return redirect()->intended('/admin/user/list');
            }
            return redirect()->intended('/user/books/list');
        }
        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]); 
    }

    /**
     * Redirect depending on the role 
     *
     * @return void
     */
    public function redirectTo() {
        if (!Auth::check()) {
            return view('auth.login');
        }

        if (Auth::check() && Auth::user()->role_id == Role::SYSTEM_ADMIN) {
            return redirect('admin/user/list');
        }

        if (Auth::check() && Auth::user()->role_id == Role::USER) {
            return redirect('user/books/list');
        }
    }
}
