<?php

namespace App\Http\Resources;

use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Resources\Json\JsonResource;

class AccountResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $createdBy = User::createdBy($this->created_by)->select(DB::raw("CONCAT(users.first_name,' ',users.last_name) AS name"))->first();
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'role_id' => $this->role,
            'created_by' => $this->created_by ? $createdBy : null
        ];
    }
}
