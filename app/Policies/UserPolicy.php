<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * User is admin
     *
     * @param User $user
     * @return void
     */
    public function admin(User $user)
    {
        return $user->isAdmin();
    }

    /**
     * User is normal user
     *
     * @param User $user
     * @return void
     */
    public function user(User $user)
    {
        return $user->isUser();
    }
}
